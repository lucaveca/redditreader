package com.example.student.redditreader;

import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;

public class RedditActivity extends SingleFragmentActivity  implements ActivityCallback {

    @LayoutRes
    @Override
    protected int getLayoutRedId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    protected Fragment createFragment() {
        return new RedditListFragment();
    }

    @Override
    public void onPostSelected(Uri redditPostUri) {
        if(findViewById(R.id.detail_fragment_container) == null) {
            Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);
            intent.setData(redditPostUri);
            startActivity(intent);
            MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.bing);
            mp.start();
        }
        else{
            Fragment detailFragment = RedditWebFragment.newfragment(redditPostUri.toString());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, detailFragment)
                    .commit();
        }
    }
}
